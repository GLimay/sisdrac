# -*- coding: utf-8 -*-
import xlwt

from django.http import HttpResponse
from apps.administracion.models import Organizacion
from apps.inicio.models import Provincia, Distrito, CentroPoblado, Caserio
from apps.agricola.models import Variedad
from datetime import datetime
from django.utils import formats
from django.db.models import Count, Min, Sum, Avg

def ListaOrganizacionesXLSX(request):

    provincia = request.POST.get('provincia', 0)
    distrito = request.POST.get('distrito', 0)
    centropoblado = request.POST.get('centropoblado', 0)

    if provincia == 0:
        text_provincia = 'TODAS'
        text_distrito = 'TODOS'
        text_centropoblado = 'TODOS'
        organizaciones = Organizacion.objects.all()

    elif provincia > 0 and distrito == 0:
        try:
            obj_provincia = Provincia.objects.get(pk=provincia)
            text_provincia = obj_provincia.nombre
        except Provincia.DoesNotExist:
            text_provincia = 'TODAS'

        text_distrito = 'TODOS'
        text_centropoblado = 'TODOS'
        organizaciones = Organizacion.objects.all().filter(caserio__centropoblado__distrito__provincia__id=provincia)
        
    elif provincia > 0 and distrito > 0 and centropoblado == 0:
        try:
            obj_provincia = Provincia.objects.get(pk=provincia)
            text_provincia = obj_provincia.nombre
        except Provincia.DoesNotExist:
            text_provincia = 'TODAS'

        try:
            obj_distrito = Distrito.objects.get(pk=distrito)
            text_distrito = obj_distrito.nombre
        except Distrito.DoesNotExist:
            text_distrito = 'TODOS'

        text_centropoblado = 'TODOS'
        organizaciones = Organizacion.objects.all().filter(
                                        caserio__centropoblado__distrito__provincia__id=provincia, 
                                        caserio__centropoblado__distrito__id=distrito
                                        )

    else:
        try:
            obj_provincia = Provincia.objects.get(pk=provincia)
            text_provincia = obj_provincia.nombre
        except Provincia.DoesNotExist:
            text_provincia = 'TODAS'

        try:
            obj_distrito = Distrito.objects.get(pk=distrito)
            text_distrito = obj_distrito.nombre
        except Distrito.DoesNotExist:
            text_distrito = 'TODOS'

        try:
            obj_centropoblado = CentroPoblado.objects.get(pk=centropoblado)
            text_centropoblado = obj_centropoblado.nombre
        except CentroPoblado.DoesNotExist:
            text_centropoblado = 'TODOS'

        organizaciones = Organizacion.objects.all().filter(
                                        caserio__centropoblado__distrito__provincia__id=provincia, 
                                        caserio__centropoblado__distrito__id=distrito,
                                        caserio__centropoblado__id=centropoblado
                                        )

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Organizacion_' + str(formats.date_format(datetime.now(), "d-m-Y")) + '.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Lista')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    data = [
                ('DIRECCIÓN REGIONAL DE AGRICULATURA CAJAMARCA',),
                ('REPORTE DE ORGANIZACIONES DE LAS CANTIDADES TOTALES DE ÁREA, RENDIMIENTO Y VOLÚMEN DEVARIEDADES',),
                ('',),
                ('PROVINCIA', text_provincia),
                ('DISTRITO', text_distrito),
                ('CENTRO POBLADO', text_centropoblado),
            ]

    for tupla in data:
        row_num = row_num + 1
        for i, col_num in enumerate(tupla):
            ws.write(row_num, i, col_num, font_style)

    #Encabezados
    row_num = row_num + 3
    columns = ['Item', 'Nombre', 'Dirección', 'RUC', 'Datos Adjuntos', 'Área', 'Rendimiento', 'Volúmen', 'Provincia', 'Distrito', 'Centro Poblado']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Llenando datos

    font_style = xlwt.XFStyle()
    for i, organizacion in enumerate(organizaciones):
        row_num = row_num + 1

        variedades = Variedad.objects.filter(socio__base__organizacion__id=organizacion.id).annotate(total_area=Sum('area'), total_rendimiento=Sum('rendimiento'), total_volumenproduccion=Sum('volumenproduccion'))

        columns = [
                        i + 1, 
                        "%s" % (organizacion.nombre),
                        "%s" % (organizacion.direccion),
                        "%s" % (organizacion.ruc),
                        "¿De donde sale esta información?",
                        variedades[0].total_area if len(variedades) > 0 else '' ,
                        variedades[0].total_rendimiento if len(variedades) > 0 else '' ,
                        variedades[0].total_volumenproduccion if len(variedades) > 0 else '',
                        '%s' % (organizacion.caserio.centropoblado.distrito.provincia.nombre),
                        '%s' % (organizacion.caserio.centropoblado.distrito.nombre),
                        '%s' % (organizacion.caserio.centropoblado.nombre),
                    ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    ws.write(row_num + 3, 0, 'Reporte generado el ' + str(formats.date_format(datetime.now(), "d-m-Y h:i:s a")), font_style)

    wb.save(response)
    return response