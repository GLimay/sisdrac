from django.forms import ModelForm
from ..models import UnidadMedida

class UnidadMedidaForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(UnidadMedidaForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = UnidadMedida
		fields = ['nombre', 'abreviatura']

