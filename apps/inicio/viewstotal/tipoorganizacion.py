from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse
import json

from ..models import AuditableMixin, TipoOrganizacion
from ..formstotal.tipoorganizacion import TipoOrganizacionForm
from ..mixin import LoginRequiredMixin

class TipoOrganizacionView(LoginRequiredMixin, TemplateView):
    template_name = "inicio/tipoorganizacion/inicio.html"

class TipoOrganizacionListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        tiposorganizaciones = TipoOrganizacion.objects.all().order_by('-id')

        if len(search) > 0:
            tiposorganizaciones = tiposorganizaciones.filter(nombre__icontains=search)
            # profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

        if sort != '':
            tiposorganizaciones = tiposorganizaciones.order_by(orderby + sort)

        paginador = Paginator(tiposorganizaciones, limit)

        try:
            tiposorganizaciones = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            tiposorganizaciones = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            tiposorganizaciones = paginador.page(paginador.num_pages)

        return render(
            request,
            'inicio/tipoorganizacion/datos.json',
            {
                'tiposorganizaciones': tiposorganizaciones,
                'total': paginador.count
            }
        )

class TipoOrganizacionCreateView(AuditableMixin, CreateView):
    model = TipoOrganizacion
    form_class = TipoOrganizacionForm
    template_name = "inicio/tipoorganizacion/formulario.html"

    def form_valid(self, form):
        super(TipoOrganizacionCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(TipoOrganizacionCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class TipoOrganizacionUpdateView(AuditableMixin, UpdateView):
    model = TipoOrganizacion
    form_class = TipoOrganizacionForm
    template_name = "inicio/tipoorganizacion/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.tipoorganizacion_id = kwargs['pk']
        return super(TipoOrganizacionUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(TipoOrganizacionUpdateView, self).form_valid(form)
        tipoorganizacion = TipoOrganizacion.objects.get(pk=self.tipoorganizacion_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(TipoOrganizacionUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(TipoOrganizacionUpdateView, self).get_context_data(**kwargs)
        context["tipoorganizacion_id"] = self.tipoorganizacion_id
        return context

class TipoOrganizacionDeleteView(DeleteView):
    model = TipoOrganizacion
    form_class = TipoOrganizacionForm
    template_name = "inicio/tipoorganizacion/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')


