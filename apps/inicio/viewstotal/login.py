# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from ...administracion.models import Persona
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
import json

def validRequest(request, metodo, variables):
    _result = True
    if request.method != metodo:
        _result = False
    else:
        for vari in variables:
            if eval("request." + metodo + ".get('" + vari + "', None) == None"):
                _result = False
                break
    return _result

def LoginView(request):
    if validRequest(request, "POST", ["username", "password"]):
        un = request.POST["username"]
        pw = request.POST["password"]
        rpta = {}
        rpta["success"] = False
        rpta["msg"] = ""
        # Buscamos a la persona:
        personas = Persona.objects.filter(
            user__username=un
        )
        usuarios = User.objects.filter(
            username=un
        )
        autenticar = False
        if personas.count() > 0:
            persona = personas[0]
            # Verificamos si el usuario es trabajador
            if persona.user.is_superuser or persona.estrabajador():
                # Validamos el estado
                if persona.user.is_active:
                    autenticar = True
                else:
                    rpta["msg"] = "El usuario se encuentra inactivo"
            else:
                rpta["msg"] = "La persona no es trabajador de la empresa"
        elif usuarios.count() > 0:
            autenticar = True
        else:
            rpta["msg"] = "El usuario no existe"
        if autenticar:
            # Validamos su usuario y contraseña:
            user = authenticate(
                username=un,
                password=pw
            )
            if user is not None:
                login(request, user)
                rpta["success"] = True
                rpta["msg"] = "Usuario Autenticado"
            else:
                rpta["msg"] = "Contraseña inválida"
        return HttpResponse(
            json.dumps(rpta, ensure_ascii=False),
            content_type="application/json; encoding=utf-8"
        )
    else:
        return HttpResponseRedirect('/')

def LogoutView(request):
    logout(request)
    request.app_menus = None
    request.app_submenus = None
    return HttpResponseRedirect("/")
