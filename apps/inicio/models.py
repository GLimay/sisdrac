# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
# Create your models here.
from django.http import HttpResponseRedirect

class Auditoria(models.Model):
	creado = models.DateTimeField(auto_now_add=True, editable=False)
	editado = models.DateTimeField(auto_now=True)
	creador = models.ForeignKey(User, related_name='+', on_delete=models.PROTECT)
	editor = models.ForeignKey(User, related_name='+', blank=True, null=True, on_delete=models.PROTECT)

	class Meta:
		abstract = True

class AuditableMixin(object,):

	def form_valid(self, form, ):
		if form.instance.id == None:
			form.instance.creador = self.request.user
		else:
			form.instance.editor = self.request.user
		return super(AuditableMixin, self).form_valid(form)

class UnidadMedida(Auditoria):
	nombre = models.CharField(max_length=100, unique=True)
	abreviatura = models.CharField(max_length=10, unique=True, default="", blank=True)

	class Meta:
		verbose_name = 'Unidad de Medida'
		verbose_name_plural = 'Unidades de Medida'

	def get_absolute_url(self):
		return ""

	def __unicode__(self):
		return self.nombre

class Mes(Auditoria):
	numero = models.SmallIntegerField(unique=True)
	nombre = models.CharField(max_length=15, unique=True)

	class Meta:
		verbose_name = 'Mes'
		verbose_name_plural = 'Meses'

	def __unicode__(self):
		return self.nombre

	def get_absolute_url(self):
		return ""

class Departamento(Auditoria):
	codigo = models.CharField(max_length=2, unique=True)
	nombre = models.CharField(max_length=45, unique=True)

	class Meta:
		verbose_name = 'Departamento'
		verbose_name_plural = 'Departamentos'

	def __unicode__(self):
		return self.nombre

	def get_absolute_url(self):
		return ""

class Provincia(Auditoria):
	codigo = models.CharField(max_length=4, unique=True)
	nombre = models.CharField(max_length=75)
	departamento = models.ForeignKey(Departamento, on_delete=models.PROTECT)

	class Meta:
		verbose_name = 'Provincia'
		verbose_name_plural = 'Provincias'

	def __unicode__(self):
		return self.nombre

	def get_absolute_url(self):
		return ""

class Distrito(Auditoria):
	codigo = models.CharField(max_length=6, unique=True)
	nombre = models.CharField(max_length=75)
	provincia = models.ForeignKey(Provincia, on_delete=models.PROTECT)

	class Meta:
		verbose_name = 'Distrito'
		verbose_name_plural = 'Distritos'

	def __unicode__(self):
		return self.nombre

	def get_absolute_url(self):
		return ""

class CentroPoblado(Auditoria):
	codigo = models.CharField(max_length=10, unique=True)
	nombre = models.CharField(max_length=45)
	area = models.CharField(max_length=15)
	distrito = models.ForeignKey(Distrito, on_delete=models.PROTECT)

	class Meta:
		verbose_name = 'Centro Poblado'
		verbose_name_plural = 'Centros Poblados'

	def __unicode__(self):
		return self.nombre

	def get_absolute_url(self):
		return ""

class Caserio(Auditoria):
	codigo = models.CharField(max_length=12, unique=True)
	nombre = models.CharField(max_length=45)
	centropoblado = models.ForeignKey(CentroPoblado, on_delete=models.PROTECT)

	class Meta:
		verbose_name = 'Caserío'
		verbose_name_plural = 'Caseríos'

	def __unicode__(self):
		return self.nombre

	def get_absolute_url(self):
		return ""

class Profesion(Auditoria):
	nombre = models.CharField(max_length=100, unique=True)

	class Meta:
		verbose_name = 'Profesión'
		verbose_name_plural = 'Profesiones'

	def get_absolute_url(self):
		return ""

	def __unicode__(self):
		return self.nombre

class Especialidad(Auditoria):
	nombre = models.CharField(max_length=100, unique=True)

	class Meta:
		verbose_name = 'Especialidad'
		verbose_name_plural = 'Especialidades'

	def __unicode__(self):
		return self.nombre

	def get_absolute_url(self):
		return ""

class TipoOrganizacion(Auditoria):
	nombre = models.CharField(max_length=75, unique=True)
	estado = models.BooleanField(default=True)

	class Meta:
		verbose_name = 'Tipo de Organización'
		verbose_name_plural = 'Tipos de Organizaciones'

	def __unicode__(self):
		return self.nombre

	def get_absolute_url(self):
		return ""

