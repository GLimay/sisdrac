# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import *

from django.contrib import admin

# Register your models here.
class AdminUnidadMedida(admin.ModelAdmin):
	list_display = ["id", "nombre", "abreviatura"]
	# list_filter = ["nombre"]
	search_fields = ["nombre", "abreviatura"]
	#list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminMes(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["numero", "nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminDepartamento(admin.ModelAdmin):
	list_display = ["id", "codigo", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["codigo", "nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminProvincia(admin.ModelAdmin):
	list_display = ["id", "codigo", "nombre", "departamento"]
	# list_filter = ["nombre"]
	search_fields = ["codigo", "nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminDistrito(admin.ModelAdmin):
	list_display = ["id", "codigo", "nombre", "provincia"]
	# list_filter = ["nombre"]
	search_fields = ["codigo", "nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminCentroPoblado(admin.ModelAdmin):
	list_display = ["id", "codigo", "nombre", "area", "distrito"]
	# list_filter = ["nombre"]
	search_fields = ["codigo", "nombre", "area"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminCaserio(admin.ModelAdmin):
	list_display = ["id", "codigo", "nombre", "centropoblado"]
	# list_filter = ["nombre"]
	search_fields = ["codigo", "nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminProfesion(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminEspecialidad(admin.ModelAdmin):
	list_display = ["id", "nombre"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

class AdminTipoOrganizacion(admin.ModelAdmin):
	list_display = ["id", "nombre", "estado"]
	# list_filter = ["nombre"]
	search_fields = ["nombre"]
	# list_editable = ["nombre"]
	# icon = '<i class="material-icons">playlist_add_check</i>'

admin.site.register(UnidadMedida, AdminUnidadMedida)
admin.site.register(Mes, AdminMes)
admin.site.register(Departamento, AdminDepartamento)
admin.site.register(Provincia, AdminProvincia)
admin.site.register(Distrito, AdminDistrito)
admin.site.register(CentroPoblado, AdminCentroPoblado)
admin.site.register(Caserio, AdminCaserio)
admin.site.register(Profesion, AdminProfesion)
admin.site.register(Especialidad, AdminEspecialidad)
admin.site.register(TipoOrganizacion, AdminTipoOrganizacion)

