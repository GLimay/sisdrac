from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import FormView, ListView, DetailView
from django.http import HttpResponse
import json

from ...inicio.models import AuditableMixin
from ..models import Raza, Especie
from ..formstotal.raza import RazaForm, RazaSelectForm
from ...inicio.mixin import LoginRequiredMixin

class RazaSelectView(LoginRequiredMixin, FormView):
    template_name = "agricola/raza/inicio.html"
    form_class = RazaSelectForm

    def get_form_kwargs(self):
        kwargs = super(RazaSelectView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(RazaSelectView, self).get_context_data(**kwargs)
        data['formEspecie'] = data.get('form')
        return data

class RazaTableView(LoginRequiredMixin, DetailView):
    template_name = "agricola/raza/raza.html"
    model = Especie
    context_object_name = "oEspecie"

class RazaListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        espid = kwargs.get('especie', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        razas = Raza.objects.filter(especie_id=espid).order_by('-id')

        if len(search) > 0:
            razas = razas.filter(
                Q(especie__nombre__icontains=search) |
                Q(nombre__icontains=search)
            )

        if sort != '':
            razas = razas.order_by(orderby + sort)

        paginador = Paginator(razas, limit)

        try:
            razas = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            razas = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            razas = paginador.page(paginador.num_pages)

        return render(
            request,
            'agricola/raza/datos.json',
            {
                'razas': razas,
                'total': paginador.count
            }
        )

class RazaCreateView(AuditableMixin, CreateView):
    model = Raza
    form_class = RazaForm
    template_name = "agricola/raza/formulario.html"

    def form_valid(self, form):
        super(RazaCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(RazaCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class RazaUpdateView(AuditableMixin, UpdateView):
    model = Raza
    form_class = RazaForm
    template_name = "agricola/raza/formulario.html"

    def form_valid(self, form):
        super(RazaUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(RazaUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class RazaDeleteView(DeleteView):
    model = Raza
    form_class = RazaForm
    template_name = "agricola/raza/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')

