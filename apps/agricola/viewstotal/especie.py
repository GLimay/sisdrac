from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormMixin
from django.views.generic import TemplateView, ListView, FormView
from django.http import HttpResponse
import json

from ...inicio.models import AuditableMixin
from ..models import Especie, EnfermedadXEspecie, Enfermedad
from ..formstotal.especie import EspecieForm, EnfermedadXEspecieForm
from ...inicio.mixin import LoginRequiredMixin

class EspecieView(LoginRequiredMixin, TemplateView):
    template_name = "agricola/especie/inicio.html"

class EspecieListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        especies = Especie.objects.all().order_by('-id')

        if len(search) > 0:
            especies = especies.filter(
                Q(nombre__icontains=search)
            )

        if sort != '':
            especies = especies.order_by(orderby + sort)

        paginador = Paginator(especies, limit)

        try:
            especies = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            especies = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            especies = paginador.page(paginador.num_pages)

        return render(
            request,
            'agricola/especie/datos.json',
            {
                'especies': especies,
                'total': paginador.count
            }
        )

class EspecieCreateView(AuditableMixin, CreateView):
    model = Especie
    form_class = EspecieForm
    template_name = "agricola/especie/formulario.html"

    def form_valid(self, form):
        super(EspecieCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(EspecieCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class EspecieUpdateView(AuditableMixin, UpdateView):
    model = Especie
    form_class = EspecieForm
    template_name = "agricola/especie/formulario.html"

    def form_valid(self, form):
        super(EspecieUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(EspecieUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class EspecieDeleteView(DeleteView):
    model = Especie
    form_class = EspecieForm
    template_name = "agricola/especie/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')

class EspecieEnfermedadesView(AuditableMixin, UpdateView):
    template_name = "agricola/especie/enfermades.html"
    model = Especie
    form_class = EnfermedadXEspecieForm
    context_object_name = "enfermedades"

    def form_valid(self, form):
        form.request = self.request
        super(EspecieEnfermedadesView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        form.request = self.request
        super(EspecieEnfermedadesView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})




