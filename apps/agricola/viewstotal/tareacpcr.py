# -*- coding: utf-8 -*-
import json

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from ...inicio.models import AuditableMixin
from ...planificacion.models import TareaCpCr, SubActividadComponente
from ..formstotal.tareacpcr import TareaCpCrForm, SubActividadCpCrForm
from ...inicio.mixin import LoginRequiredMixin


class TareaCpCrView(LoginRequiredMixin, TemplateView):
    template_name = "agricola/tareacpcr/inicio.html"
    form_class = SubActividadCpCrForm

    def get_context_data(self, **kwargs):
        context = super(TareaCpCrView, self).get_context_data(**kwargs)
        context['form'] = self.form_class
        return context

class TareaCpCrListView(LoginRequiredMixin, ListView):
    def get(self, request, *args, **kwargs):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        subactid = request.GET.get('subactividadcomponente', 0)
        orderby = ""
        if order != 'asc':
            orderby = "-"

        tareascpcr = TareaCpCr.objects.filter(subactividadcomponente_id=subactid).order_by('-id')

        if len(search) > 0:
            tareascpcr = tareascpcr.filter(
                Q(persona__apepat__icontains=search) |
                Q(persona__apemat__icontains=search) |
                Q(persona__nombres__icontains=search)
            )

        if sort != '':
            tareascpcr = tareascpcr.order_by(orderby + sort)

        paginador = Paginator(tareascpcr, limit)

        try:
            tareascpcr = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            tareascpcr = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            tareascpcr = paginador.page(paginador.num_pages)

        return render(
            request,
            'agricola/tareacpcr/datos.json',
            {
                'tareascpcr': tareascpcr,
                'total': paginador.count
            }
        )


class TareaCpCrCreateView(AuditableMixin, CreateView):
    model = TareaCpCr
    form_class = TareaCpCrForm
    template_name = "agricola/tareacpcr/formulario.html"

    def form_valid(self, form):
        super(TareaCpCrCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(TareaCpCrCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})


class TareaCpCrUpdateView(AuditableMixin, UpdateView):
    model = TareaCpCr
    form_class = TareaCpCrForm
    template_name = "agricola/tareacpcr/formulario.html"

    def form_valid(self, form):
        super(TareaCpCrUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(TareaCpCrUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})


class TareaCpCrDeleteView(DeleteView):
    model = TareaCpCr
    form_class = TareaCpCrForm
    template_name = "agricola/tareacpcr/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')

