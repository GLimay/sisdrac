# -*- coding: utf-8 -*-
import xlwt

from django.http import HttpResponse
from ..models import Base
from datetime import datetime
from django.utils import formats

def ListaBasesXLS(request):

    bases = enumerate(Base.objects.all().order_by('-id'))

    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Bases_' + str(formats.date_format(datetime.now(), "d-m-Y")) + '.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Lista')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    
    #Encabezado
    row_num = row_num + 1
    columns = ['', '', '', 'Dirección Regional de Agricultura Cajamarca', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    row_num = row_num + 1
    columns = ['', '', '', 'SisDRAC', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    row_num = row_num + 1
    columns = ['', '', '', 'Lista de Bases', '', '', '', '', '', '']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Encabezados
    row_num = row_num + 3
    columns = ['Nro', 'Nombre', 'Organización', 'Departamento', 'Provincia', 'Distrito', 'Centro Poblado', 'Caserío', 'Lugar']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    #Llenando datos
    font_style = xlwt.XFStyle()
    for i, base in bases:
        row_num = row_num + 1

        columns = [
                        i + 1, 
                        "%s" % (base.nombre), 
                        "%s" % (base.organizacion),
                        "%s" % (base.caserio.centropoblado.distrito.provincia.departamento.nombre),
                        "%s" % (base.caserio.centropoblado.distrito.provincia.nombre),
                        "%s" % (base.caserio.centropoblado.distrito.nombre),
                        "%s" % (base.caserio.centropoblado.nombre),
                        "%s" % (base.caserio.nombre),
                        "%s" % (base.Caserio()),
                    ]

        for col_num in range(len(columns)):
            ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    ws.write(row_num + 3, 2, 'Reporte generado el ' + str(formats.date_format(datetime.now(), "d-m-Y h:i:s a")), font_style)

    wb.save(response)
    return response