from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse
import json
from ...inicio.models import AuditableMixin
from ..models import RazaXSocio, Socio
from ..formstotal.razasocio import RazaSocioForm, SociosForm
from ...inicio.mixin import LoginRequiredMixin

class RazaSocioView(LoginRequiredMixin, TemplateView):
    template_name = "agricola/razasocio/inicio.html"
    form_class = SociosForm

    def get_context_data(self, **kwargs):
        context = super(RazaSocioView, self).get_context_data(**kwargs)
        context['form'] = self.form_class
        return context

class RazaSocioDetailView(LoginRequiredMixin, DetailView):
    template_name = "agricola/razasocio/socio.html"
    context_object_name = "socio"
    model = Socio

class RazaSocioListView(LoginRequiredMixin, ListView):

    def get(self, request):

        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        idsocio = int(request.GET.get('socio', -1))
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        razasocio = RazaXSocio.objects.filter(socio_id=idsocio).order_by('-id')

        if len(search) > 0:
            # socios = socios.filter(nombre__icontains=search)
            razasocio = razasocio.filter(
                Q(rendimiento__icontains=search) |
                Q(nrocabezas__icontains=search) |
                Q(produccion__icontains=search)
            )

        if sort != '':
            razasocio = razasocio.order_by(orderby + sort)

        paginador = Paginator(razasocio, limit)

        try:
            razasocio = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            razasocio = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            razasocio = paginador.page(paginador.num_pages)

        return render(
            request,
            'agricola/razasocio/datos.json',
            {
                'razasocios': razasocio,
                'total': paginador.count
            }
        )


class RazaSocioCreateView(AuditableMixin, CreateView):

    model = RazaXSocio
    form_class = RazaSocioForm
    template_name = "agricola/razasocio/formulario.html"

    def form_valid(self, form):
        super(RazaSocioCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(RazaSocioCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class RazaSocioUpdateView(AuditableMixin, UpdateView):

    model = RazaXSocio
    form_class = RazaSocioForm
    template_name = "agricola/razasocio/formulario.html"

    def form_valid(self, form):
        super(RazaSocioUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(RazaSocioUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class RazaSocioDeleteView(DeleteView):

    model = RazaXSocio
    form_class = RazaSocioForm
    template_name = "agricola/razasocio/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')

