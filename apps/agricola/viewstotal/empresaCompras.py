from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import ListView,TemplateView
from django.http import HttpResponse
import json

from ...inicio.models import AuditableMixin
from ..models import Empresa
from ..formstotal.formEmpresaCompras import formEmpresaCompras
from ...inicio.mixin import LoginRequiredMixin

class EmpresaTableView(LoginRequiredMixin, TemplateView):
    template_name = "agricola/empresa/empresa_index.html"

class EmpresaListView(LoginRequiredMixin, ListView):
    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        empresa = Empresa.objects.all()


        if len(search) > 0:
            empresa = empresa.filter(
                Q(razon_social__icontains=search) |
                Q(ruc__icontains=search)
            )

        if sort != '':
            empresa = empresa.order_by(orderby + sort)

        paginador = Paginator(empresa, limit)

        try:
            empresa = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            empresa = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            empresa = paginador.page(paginador.num_pages)

        return render(
            request,
            'agricola/empresa/empresa.json',
            {
                'empresa': empresa,
                'total': paginador.count
            }
        )

class EmpresaCreateView(AuditableMixin, CreateView):
    model = Empresa
    form_class = formEmpresaCompras
    template_name = "agricola/empresa/fromEmpresa.html"

    def form_valid(self, form):
        super(EmpresaCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(EmpresaCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class EmpresaUpdateView(AuditableMixin, UpdateView):
    model = Empresa
    form_class = formEmpresaCompras
    template_name = "agricola/empresa/fromEmpresa.html"

    def form_valid(self, form):
        super(EmpresaUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(EmpresaUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class EmpresaDeleteView(DeleteView):
    model = Empresa
    form_class = formEmpresaCompras
    template_name = "agricola/empresa/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')

