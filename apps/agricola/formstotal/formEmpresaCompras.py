
from django import forms
from ..models import Empresa

class formEmpresaCompras(forms.ModelForm):

	class Meta:
		model = Empresa
		fields = ['razon_social', 'ruc']

	def __init__(self, *args, **kwargs):
		super(formEmpresaCompras, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

