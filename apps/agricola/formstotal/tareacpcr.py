# -*- coding: utf-8 -*-
from datetimewidget.widgets import DateWidget
from django import forms
from django.utils.encoding import force_text
from django_select2.forms import ModelSelect2Widget

from ...administracion.models import Trabajador
from ..models import Base
from ...planificacion.models import TareaCpCr, CadenaProductiva, Actividad, SubActividadComponente


class SubActividadCpCrForm(forms.Form):
    cadenaproductiva = forms.ModelChoiceField(
        required=False,
        queryset=CadenaProductiva.objects.filter(tipo='CR'),
        label=u'Cadena Productiva',
        widget=ModelSelect2Widget(
            search_fields=['nombre__icontains'],
            model = CadenaProductiva,
        )
    )

    actividad = forms.ModelChoiceField(
        required=False,
        queryset=Actividad.objects.all(),
        label=u'Actividad',
        widget=ModelSelect2Widget(
            search_fields=['nombre__icontains'],
            model = Actividad,
            dependent_fields={'cadenaproductiva': 'cadenaproductiva'},
        )
    )

    subactividadcomponentes = forms.ModelChoiceField(
        required=False,
        queryset=SubActividadComponente.objects.all(),
        label=u'Sub Actividad',
        widget=ModelSelect2Widget(
            search_fields=['nombre__icontains'],
            model = SubActividadComponente,
            dependent_fields={'actividad': 'actividad'},
        )
    )


class TareaCpCrForm(forms.ModelForm):

    base = forms.ModelChoiceField(
        label=u"Base",
        queryset=Base.objects.all(),
        widget=ModelSelect2Widget(
            queryset=Base.objects.order_by("nombre"),
            search_fields=["nombre__icontains"],
            max_results=10
        )
    )

    inicio = forms.DateField(
        label=u'Inicio',
        required=True,
        widget=DateWidget(
            usel10n=True,
            bootstrap_version=3,
            attrs={'data-datepicker-type': "4", "data-provide": "datepicker-inline"},
            options={
                'autoclose': 'true',
                'clearBtn': 'false'
            },
        )
    )

    fin = forms.DateField(
        label=u'Finalización',
        required=True,
        widget=DateWidget(
            usel10n=True,
            bootstrap_version=3,
            attrs={'data-datepicker-type': "4", "data-provide": "datepicker-inline"},
            options={
                'autoclose': 'true',
                'clearBtn': 'false'
            },
        )
    )

    class Meta:
        model = TareaCpCr
        fields = [
            'nombre', 'prioridad', 'porcentaje_completado', 'descripcion', 'inicio', 'fin', 'evidencia',
            'adjunto', 'persona', 'subactividadcomponente', 'base', 'tema', 'metodologia', 'desarrollo_tema',
            'requerimiento', 'compromiso', 'estado'
        ]

    def __init__(self, *args, **kwargs):
        super(TareaCpCrForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            field.widget.attrs['class'] = 'form-control'
        self.fields['descripcion'].label = 'Descripción'
        self.fields['subactividadcomponente'].widget = forms.HiddenInput()

