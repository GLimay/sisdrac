from django.forms import ModelForm
from django import forms
from django_select2.forms import ModelSelect2Widget

from apps.administracion.models import Organizacion
from apps.inicio.models import Departamento, Provincia, Distrito, CentroPoblado, Caserio
from ..models import Base

class BaseForm(ModelForm):

	departamento = forms.ModelChoiceField(
		queryset=Departamento.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			queryset=Departamento.objects.order_by("nombre"),
		)
	)

	provincia = forms.ModelChoiceField(
		queryset=Provincia.objects.all(),
		widget=ModelSelect2Widget(
			queryset=Provincia.objects.order_by("nombre"),
			search_fields=['nombre__icontains'],
			dependent_fields={'departamento':'departamento'},
			max_results=10,
		)
	)

	distrito = forms.ModelChoiceField(
		queryset=Distrito.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'provincia': 'provincia'},
			max_results=10,
			queryset=Distrito.objects.order_by("nombre"),
		)
	)

	centropoblado = forms.ModelChoiceField(
		queryset=CentroPoblado.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'distrito': 'distrito'},
			max_results=10,
			queryset=CentroPoblado.objects.order_by("nombre"),
		)
	)

	caserio = forms.ModelChoiceField(
		queryset=Caserio.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			dependent_fields={'centropoblado': 'centropoblado'},
			max_results=10,
			queryset=Caserio.objects.order_by("nombre"),
		)
	)

	organizacion = forms.ModelChoiceField(
		queryset=Organizacion.objects.all(),
		widget=ModelSelect2Widget(
			search_fields=['nombre__icontains'],
			queryset=Organizacion.objects.order_by("nombre"),
		)
	)

	class Meta:
		model = Base
		fields = [
			'nombre', 'departamento', 'provincia', 'distrito', 'centropoblado',
			'caserio', 'organizacion'
		]

	def __init__(self, *args, **kwargs):
		super(BaseForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'
		if self.instance.pk != None:
			self.fields["departamento"].initial = self.instance.caserio.centropoblado.distrito.provincia.departamento
			self.fields["provincia"].initial = self.instance.caserio.centropoblado.distrito.provincia
			self.fields["distrito"].initial = self.instance.caserio.centropoblado.distrito
			self.fields["centropoblado"].initial = self.instance.caserio.centropoblado

