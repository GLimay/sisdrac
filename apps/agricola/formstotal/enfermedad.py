from django.forms import ModelForm
from ..models import Enfermedad

class EnfermedadForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(EnfermedadForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = Enfermedad
		fields = ['nombre', 'descripcion']
