# -*- coding: utf-8 -*-
import datetime
from django import forms
from django_select2.forms import ModelSelect2Widget
from datetimewidget.widgets import DateWidget

from sisagro.widgets.inputcheck import ModelCheckWidget
from ...administracion.models import Persona
from ..models import Socio, Base

class SocioSelectForm(forms.Form):
    opbase = forms.ChoiceField(
        label=u'Base',
        required=False,
        widget=ModelSelect2Widget(
            attrs={'id': 'cb_base', 'class': 'formBase_load', 'data-allow-clear': 'false'},
            queryset=Base.objects.all().order_by("nombre"),
            model=Base,
            search_fields=['nombre__icontains'],
            max_results=10,
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(SocioSelectForm, self).__init__(*args, **kwargs)

class SocioForm(forms.ModelForm):
    estado = ModelCheckWidget(label="Estado", textYES="Activo", textNO="Inactivo", initial=True)

    baseid = forms.IntegerField(
        widget=forms.HiddenInput
    )

    persona = forms.ModelChoiceField(
        queryset=Persona.objects.all(),
        label=u'Persona',
        widget=ModelSelect2Widget(
            model=Persona,
            queryset=Persona.objects.all().order_by("apepat", "apemat", "nombres"),
            search_fields=['nombres__icontains', 'apepat__icontains', 'apemat__icontains', 'dni__icontains'],
            max_results=10,
        )
    )

    ingreso = forms.DateField(
        label=u'Ingreso',
        required=True,
        widget=DateWidget(
            usel10n=True,
            bootstrap_version=3,
            attrs={'data-datepicker-type': "4", "data-provide": "datepicker-inline"},
            options={
                'autoclose': 'true',
                'clearBtn': 'false'
            },
        )
    )

    salida = forms.DateField(
        label=u'Salida',
        required=False,
        widget=DateWidget(
            usel10n=True,
            bootstrap_version=3,
            attrs={'data-datepicker-type': "4", "data-provide": "datepicker-inline"},
            options={
                'autoclose': 'true'
            }
        )
    )

    def __init__(self, *args, **kwargs):
        super(SocioForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            field.widget.attrs['class'] = 'form-control'
        self.fields['email'].label = 'Correo'
        self.fields['telefono'].label = 'Teléfono'

    class Meta:
        model = Socio
        fields = [
            'baseid', 'persona', 'email', 'telefono',
            'ingreso', 'salida', 'estado'
        ]

    def clean(self):
        self.instance.base_id = self.cleaned_data["baseid"]
        if self.cleaned_data.get("persona", None) != None:
            socioEnBase = Socio.objects.filter(
                base_id=self.instance.base_id,
                persona=self.cleaned_data["persona"]
            ).exclude(
                pk=self.instance.pk
            )
            if socioEnBase.count() > 0:
                self.add_error("persona", u"La persona ya se encuentra como socio en esta base")

