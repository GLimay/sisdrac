# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models.functions import datetime

from ..inicio.models import Auditoria, Caserio
from ..administracion.models import Persona, Organizacion


# Create your models here.
class Enfermedad(Auditoria):
    nombre = models.CharField(max_length=200, unique=True)
    descripcion = models.TextField()

    class Meta:
        verbose_name = 'Enfermedad'
        verbose_name_plural = 'Enfermedades'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class Especie(Auditoria):
    nombre = models.CharField(max_length=255, unique=True)
    estado = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Especie'
        verbose_name_plural = 'Especies'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""

    def Enfermedades(self):
        return self.enfermedadxespecie_set.all().count()


class EnfermedadXEspecie(Auditoria):
    enfermedad = models.ForeignKey(Enfermedad, on_delete=models.PROTECT)
    especie = models.ForeignKey(Especie, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Enfermedad por Especie'
        verbose_name_plural = 'Enfermedades por Especies'

    def __unicode__(self):
        return self.enfermedad.nombre + ' - ' + self.especie.nombre

    def get_absolute_url(self):
        return ""


class Base(Auditoria):
    nombre = models.CharField(max_length=45, unique=True)
    caserio = models.ForeignKey(Caserio, on_delete=models.PROTECT)
    organizacion = models.ForeignKey(Organizacion, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Base'
        verbose_name_plural = 'Bases'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""

    def Caserio(self):
        return self.caserio.centropoblado.distrito.provincia.departamento.nombre + ' / ' + \
               self.caserio.centropoblado.distrito.provincia.nombre + ' / ' + \
               self.caserio.centropoblado.distrito.nombre + ' / ' + \
               self.caserio.centropoblado.nombre + ' / ' + \
               self.caserio.nombre


class Socio(Auditoria):
    persona = models.ForeignKey(Persona, on_delete=models.PROTECT)
    email = models.EmailField(max_length=75, blank=True, null=True)
    telefono = models.CharField(max_length=45, blank=True, null=True)
    base = models.ForeignKey(Base, on_delete=models.PROTECT)
    ingreso = models.DateField()
    salida = models.DateField(blank=True, null=True)
    estado = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Socio'
        verbose_name_plural = 'Socios'

    def __unicode__(self):
        return self.persona.nombreCompleto()

    def get_absolute_url(self):
        return ""


class Plaga(Auditoria):
    nombre = models.CharField(max_length=45, unique=True)
    nombrecientifico = models.CharField(max_length=45, unique=True)
    descripcion = models.TextField()

    class Meta:
        verbose_name = 'Plaga'
        verbose_name_plural = 'Plagas'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class Raza(Auditoria):
    nombre = models.CharField(max_length=75, unique=True)
    especie = models.ForeignKey(Especie, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Raza'
        verbose_name_plural = 'Razas'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class RazaXSocio(Auditoria):
    socio = models.ForeignKey(Socio, on_delete=models.PROTECT)
    raza = models.ForeignKey(Raza, on_delete=models.PROTECT)
    rendimiento = models.DecimalField(max_digits=11, decimal_places=2, blank=True, null=True)
    nrocabezas = models.DecimalField(max_digits=11, decimal_places=2, blank=True, null=True)
    produccion = models.DecimalField(max_digits=11, decimal_places=2, blank=True, null=True)
    seca = models.DecimalField(max_digits=11, decimal_places=2, blank=True, null=True)

    class Meta:
        verbose_name = 'Raza por Socio'
        verbose_name_plural = 'Razas por Socios'

    def __unicode__(self):
        return self.raza.nombre + ' - ' + self.socio.persona.nombreCompleto()

    def get_absolute_url(self):
        return ""


class EnfermedadXSocio(Auditoria):
    enfermedadxespecie = models.ForeignKey(EnfermedadXEspecie, on_delete=models.PROTECT)
    razaxsocio = models.ForeignKey(RazaXSocio, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Enfermedad por Socio'
        verbose_name_plural = 'Enfermedades por Socios'

    def __unicode__(self):
        return self.enfermedadxespecie.enfermedad.nombre + ' - ' + self.razaxsocio.raza.nombre

    def get_absolute_url(self):
        return ""


class VariedadCatalogo(Auditoria):
    nombre = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name = 'Catálogo de Variedad'
        verbose_name_plural = 'Catálogos de Variedades'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class Variedad(Auditoria):
    tipoCertificado = (
        ('CO', u'Certificación Orgánica'),
        ('CC', u'Certificación Comercio Justo'),
        ('CR', u'Certificación Rinfort Alice')
    )

    tipoTitulo = (
        ('CT', u'Con Título'),
        ('ST', u'Sin Título')
    )

    variedadcatalogo = models.ForeignKey(VariedadCatalogo)
    area = models.DecimalField(max_digits=9, decimal_places=2)
    areasecano = models.DecimalField(max_digits=11, decimal_places=2)
    areabajoriego = models.DecimalField(max_digits=11, decimal_places=2)
    preciochacra = models.DecimalField(max_digits=11, decimal_places=2)
    preciomercado = models.DecimalField(max_digits=11, decimal_places=2)
    rendimiento = models.DecimalField(max_digits=9, decimal_places=2)
    volumenproduccion = models.DecimalField(max_digits=9, decimal_places=2)
    certificadas = models.DecimalField(max_digits=11, decimal_places=2)
    sincertificar = models.DecimalField(max_digits=11, decimal_places=2)
    tipocertificado = models.CharField(max_length=2, default=tipoCertificado[0][0])
    titulo = models.CharField(max_length=2, default=tipoTitulo[0][0])
    plaga = models.ForeignKey(Plaga, on_delete=models.PROTECT)
    socio = models.ForeignKey(Socio, on_delete=models.PROTECT)
    fecha = models.DateField()
    observaciones = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = 'Variedad'
        verbose_name_plural = 'Variedades'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""

    def TipoCertificado(self):
        _result = ""
        for op in self.tipoCertificado:
            if op[0] == self.tipocertificado:
                _result = op[1]
                break
        return _result

    def Titulo(self):
        _result = ""
        for op in self.tipoTitulo:
            if op[0] == self.titulo:
                _result = op[1]
                break
        return _result


class Empresa(Auditoria):
    razon_social = models.CharField(max_length=300)
    ruc = models.CharField(max_length=11)

    def __unicode__(self):
        return self.razon_social

    def get_absolute_url(self):
        return ""


class Venta(Auditoria):
    fecha = models.DateField(auto_now=True, blank=True, null=True)
    estado = models.BooleanField(default=True)
    empresa = models.ForeignKey(Empresa)

    def __unicode__(self):
        return self.empresa


class DetalleVenta(Auditoria):
    venta = models.ForeignKey(Venta)
    variedad = models.ForeignKey(Variedad)
    precio = models.DecimalField(max_digits=11, decimal_places=2)
    volumen = models.DecimalField(max_digits=11, decimal_places=2)
