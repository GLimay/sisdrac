from django.conf.urls import url
from .viewstotal import socio as soc
from .viewstotal import enfermedad, base
from .viewstotal import variedadcatalogo as varicat
from .viewstotal import empresaCompras as empresa
from .viewstotal import variedad as vari
from .viewstotal import plaga as plaga
from .viewstotal import especie as espe
from .viewstotal import raza
from .viewstotal import razasocio
from .viewstotal import tareacpcu
from .viewstotal import tareacpcr
from .viewstotal.enfermedadXLSX import *
from .viewstotal.baseXLSX import *
from .viewstotal.variedadXLSX import *
from .viewstotal.variedadsocioXLSX import *
from .viewstotal.socioXLSX import *

urlpatterns = [
    # url(r'^modelo/accion/$', ModeloAccion, name='modelo_accion'),

    # SOCIO
    url(r'^socio/$', soc.SocioSelectView.as_view(), name='socio_inicio'),
    url(r'^socio/tabla/(?P<pk>\d+)$', soc.SocioView.as_view(), name='socio_tabla'),
    url(r'^socio/listar/(?P<base>\d+)$', soc.SocioListView.as_view(), name='socio_listar'),
    url(r'^socio/crear/$', soc.SocioCreateView.as_view(), name='socio_crear'),
    url(r'^socio/actualizar/(?P<pk>\d+)$', soc.SocioUpdateView.as_view(), name='socio_actualizar'),
    url(r'^socio/eliminar/(?P<pk>\d+)$', soc.SocioDeleteView.as_view(), name='socio_eliminar'),
    url(r'^socio/xlsx/(?P<inicio>\d{2}-\d{2}-\d{4})/(?P<fin>\d{2}-\d{2}-\d{4})/$', ListaSociosXLS, name='socio_xlsx'),

    # ENFERMEDAD
    url(r'^enfermedad/$', enfermedad.EnfermedadView.as_view(), name='enfermedad_inicio'),
    url(r'^enfermedad/listar/$', enfermedad.EnfermedadListView.as_view(), name='enfermedad_listar'),
    url(r'^enfermedad/crear/$', enfermedad.EnfermedadCreateView.as_view(), name='enfermedad_crear'),
    url(r'^enfermedad/actualizar/(?P<pk>\d+)$', enfermedad.EnfermedadUpdateView.as_view(), name='enfermedad_actualizar'),
    url(r'^enfermedad/eliminar/(?P<pk>\d+)$', enfermedad.EnfermedadDeleteView.as_view(), name='enfermedad_eliminar'),
    url(r'^enfermedad/xlsx/$', ListaEnfermedadesXLS, name='enfermedad_xlsx'),
    # BASE
    url(r'^base/$', base.BaseView.as_view(), name='base_inicio'),
    url(r'^base/listar/$', base.BaseListView.as_view(), name='base_listar'),
    url(r'^base/crear/$', base.BaseCreateView.as_view(), name='base_crear'),
    url(r'^base/actualizar/(?P<pk>\d+)$', base.BaseUpdateView.as_view(), name='base_actualizar'),
    url(r'^base/eliminar/(?P<pk>\d+)$', base.BaseDeleteView.as_view(), name='base_eliminar'),
    url(r'^base/xlsx/$', ListaBasesXLS, name='base_xlsx'),

    # VARIEDAD
    url(r'^variedad/$', vari.VariedadBaseView.as_view(), name='variedad_inicio'),
    url(r'^variedad/socio/$', vari.VariedadSocioView.as_view(), name='variedad_socio'),
    url(r'^variedad/tabla/(?P<pk>\d+)$', vari.VariedadTablaView.as_view(), name='variedad_tabla'),
    url(r'^variedad/listar/(?P<socid>\d+)$', vari.VariedadListView.as_view(), name='variedad_listar'),
    url(r'^variedad/crear/$', vari.VariedadCreateView.as_view(), name='variedad_crear'),
    url(r'^variedad/actualizar/(?P<pk>\d+)$', vari.VariedadUpdateView.as_view(), name='variedad_actualizar'),
    url(r'^variedad/eliminar/(?P<pk>\d+)$', vari.VariedadDeleteView.as_view(), name='variedad_eliminar'),
    url(r'^variedad/xlsx/(?P<tipocertificacion>[\w\-]+)/(?P<provincia>\d+)/(?P<distrito>\d+)/(?P<centropoblado>\d+)/(?P<caserio>\d+)/$', ListaVariedadesXLS, name='variedad_xlsx'),
    url(r'^variedad/socio/(?P<idsocio>\d+)/xlsx/$', ListaVariedadSocioXLS, name='variedad_socio_xlsx'),

    # PLAGA
    url(r'^plaga/$', plaga.PlagaView.as_view(), name='plaga_inicio'),
    url(r'^plaga/listar/$', plaga.PlagaListView.as_view(), name='plaga_listar'),
    url(r'^plaga/crear/$', plaga.PlagaCreateView.as_view(), name='plaga_crear'),
    url(r'^plaga/actualizar/(?P<pk>\d+)$', plaga.PlagaUpdateView.as_view(), name='plaga_actualizar'),
    url(r'^plaga/eliminar/(?P<pk>\d+)$', plaga.PlagaDeleteView.as_view(), name='plaga_eliminar'),

    # ESPECIE
    url(r'^especie/$', espe.EspecieView.as_view(), name='especie_inicio'),
    url(r'^especie/listar/$', espe.EspecieListView.as_view(), name='especie_listar'),
    url(r'^especie/crear/$', espe.EspecieCreateView.as_view(), name='especie_crear'),
    url(r'^especie/actualizar/(?P<pk>\d+)$', espe.EspecieUpdateView.as_view(), name='especie_actualizar'),
    url(r'^especie/eliminar/(?P<pk>\d+)$', espe.EspecieDeleteView.as_view(), name='especie_eliminar'),
    url(r'^especie/(?P<pk>\d+)/enfermedades$', espe.EspecieEnfermedadesView.as_view(), name='especie_enfermedades'),
    # url(r'^especie/enfermedades/actualizar/(?P<pk>\d+)$', espe.EspecieEnfermedadesUpdateView.as_view(), name='especie_enfermedades_actualizar'),

    # RAZA
    url(r'^raza/$', raza.RazaSelectView.as_view(), name='raza_inicio'),
    url(r'^raza/tabla/(?P<pk>\d+)$', raza.RazaTableView.as_view(), name='raza_tabla'),
    url(r'^raza/listar/(?P<especie>\d+)$', raza.RazaListView.as_view(), name='raza_listar'),
    url(r'^raza/crear/$', raza.RazaCreateView.as_view(), name='raza_crear'),
    url(r'^raza/actualizar/(?P<pk>\d+)$', raza.RazaUpdateView.as_view(), name='raza_actualizar'),
    url(r'^raza/eliminar/(?P<pk>\d+)$', raza.RazaDeleteView.as_view(), name='raza_eliminar'),

    # RAZASOCIO
    url(r'^razasocio/$', razasocio.RazaSocioView.as_view(), name='razasocio_inicio'),
    url(r'^razasocio/listar/$', razasocio.RazaSocioListView.as_view(), name='razasocio_listar'),
    url(r'^razasocio/crear/$', razasocio.RazaSocioCreateView.as_view(), name='razasocio_crear'),
    url(r'^razasocio/actualizar/(?P<pk>\d+)$', razasocio.RazaSocioUpdateView.as_view(), name='razasocio_actualizar'),
    url(r'^razasocio/eliminar/(?P<pk>\d+)$', razasocio.RazaSocioDeleteView.as_view(), name='razasocio_eliminar'),
    url(r'^razasocio/socio/(?P<pk>\d+)$', razasocio.RazaSocioDetailView.as_view(), name='razasocio_detailview'),

    # TAREAS CULTIVOS
    url(r'^tareacpcu/$', tareacpcu.TareaCpCuCadProdView.as_view(), name='tareacpcu_inicio'),
    url(r'^tareacpcu/act/$', tareacpcu.TareaCpCuActividadView.as_view(), name='tareacpcu_actividad'),
    url(r'^tareacpcu/subact/$', tareacpcu.TareaCpCuSubActividadView.as_view(), name='tareacpcu_subactividad'),
    url(r'^tareacpcu/tabla/(?P<pk>\d+)$', tareacpcu.TareaCpCuTablaView.as_view(), name='tareacpcu_tabla'),
    url(r'^tareacpcu/listar/(?P<subact>\d+)$', tareacpcu.TareaCpCuListView.as_view(), name='tareacpcu_listar'),
    url(r'^tareacpcu/crear/$', tareacpcu.TareaCpCuCreateView.as_view(), name='tareacpcu_crear'),
    url(r'^tareacpcu/actualizar/(?P<pk>\d+)$', tareacpcu.TareaCpCuUpdateView.as_view(), name='tareacpcu_actualizar'),
    url(r'^tareacpcu/eliminar/(?P<pk>\d+)$', tareacpcu.TareaCpCuDeleteView.as_view(), name='tareacpcu_eliminar'),

    # TAREAS CRIANZA
    url(r'^tareacpcr/$', tareacpcr.TareaCpCrView.as_view(), name='tareacpcr_inicio'),
    url(r'^tareacpcr/listar/$', tareacpcr.TareaCpCrListView.as_view(), name='tareacpcr_listar'),
    url(r'^tareacpcr/crear/$', tareacpcr.TareaCpCrCreateView.as_view(), name='tareacpcr_crear'),
    url(r'^tareacpcr/actualizar/(?P<pk>\d+)$', tareacpcr.TareaCpCrUpdateView.as_view(), name='tareacpcr_actualizar'),
    url(r'^tareacpcr/eliminar/(?P<pk>\d+)$', tareacpcr.TareaCpCrDeleteView.as_view(), name='tareacpcr_eliminar'),

    # CATALOGO DE VARIEDADES
    url(r'^variedadcatalogo/$', varicat.VariedadCatalogoView.as_view(), name='variedadcatalogo_inicio'),
    url(r'^variedadcatalogo/listar/$', varicat.VariedadCatalogoListView.as_view(), name='variedadcatalogo_listar'),
    url(r'^variedadcatalogo/crear/$', varicat.VariedadCatalogoCreateView.as_view(), name='variedadcatalogo_crear'),
    url(r'^variedadcatalogo/actualizar/(?P<pk>\d+)$', varicat.VariedadCatalogoUpdateView.as_view(), name='variedadcatalogo_actualizar'),
    url(r'^variedadcatalogo/eliminar/(?P<pk>\d+)$', varicat.VariedadCatalogoDeleteView.as_view(), name='variedadcatalogo_eliminar'),


    # ENPRESA COMPRA
    url(r'^empresa/$', empresa.EmpresaTableView.as_view(), name='empresa_inicio'),
    url(r'^empresa/listar/$', empresa.EmpresaListView .as_view(), name='empresa_listar'),
    url(r'^empresa/crear/$', empresa.EmpresaCreateView.as_view(), name='empresa_crear'),
    url(r'^empresa/actualizar/(?P<pk>\d+)$', empresa.EmpresaUpdateView.as_view(), name='empresa_actualizar'),
    url(r'^empresa/eliminar/(?P<pk>\d+)$', empresa.EmpresaDeleteView.as_view(), name='empresa_eliminar'),

    # url(r'^persona/$', empresa.PersonaView.as_view(), name='persona_inicio'),
    # url(r'^persona/listar/$', empresa.PersonaListView.as_view(), name='persona_listar'),
    # url(r'^persona/crear/$', empresa.PersonaCreateView.as_view(), name='persona_crear'),
    # url(r'^persona/actualizar/(?P<pk>\d+)$', empresa.PersonaUpdateView.as_view(), name='persona_actualizar'),
    # url(r'^persona/eliminar/(?P<pk>\d+)$', empresa.PersonaDeleteView.as_view(), name='persona_eliminar'),

]
