from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse, HttpResponseRedirect
import json
from django.db.models import Q
from ...inicio.models import AuditableMixin
from ..models import Trabajador
from ..formstotal.trabajador import TrabajadorForm
from ...inicio.mixin import LoginRequiredMixin


class TrabajadorView(LoginRequiredMixin, TemplateView):
    template_name = "administracion/trabajador/inicio.html"


class TrabajadorListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        trabajador = Trabajador.objects.all().order_by('-id')

        if len(search) > 0:
            # personas = personas.filter(nombre__icontains=search)
            trabajador = Trabajador.objects.filter(
                Q(area__nombre__icontains=search) | 
                Q(persona__nombres__icontains=search) | 
                Q(persona__apepat__icontains=search) | 
                Q(persona__apemat__icontains=search) | 
                Q(profesion__nombre__icontains=search) | 
                Q(especialidad__nombre__icontains=search) | 
                Q(funcioncargo__nombre__icontains=search) | 
                Q(persona__dni__icontains=search)
            )

        if sort != '':
            trabajador = trabajador.order_by(orderby + sort)

        paginador = Paginator(trabajador, limit)

        try:
            trabajador = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            trabajador = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            trabajador = paginador.page(paginador.num_pages)

        return render(
            request,
            'administracion/trabajador/datos.json',
            {
                'trabajador': trabajador,
                'total': paginador.count
            }
        )


class TrabajadorCreateView(AuditableMixin, CreateView):
    model = Trabajador
    form_class = TrabajadorForm
    template_name = "administracion/trabajador/formulario.html"

    def form_valid(self, form):
        super(TrabajadorCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        print("invalid: ", form.cleaned_data)
        super(TrabajadorCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class TrabajadorUpdateView(AuditableMixin, UpdateView):
    model = Trabajador
    form_class = TrabajadorForm
    template_name = "administracion/trabajador/formulario.html"

    def form_valid(self, form):
        super(TrabajadorUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(TrabajadorUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class TrabajadorDeleteView(DeleteView):
    model = Trabajador
    form_class = TrabajadorForm
    template_name = "administracion/trabajador/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')

