from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse, HttpResponseRedirect
import json
from django.db.models import Q
from ...inicio.models import AuditableMixin
from ..models import Persona
from ..formstotal.persona import PersonaForm
from ...inicio.mixin import LoginRequiredMixin

class PersonaView(LoginRequiredMixin, TemplateView):

	template_name = "administracion/persona/inicio.html"


class PersonaListView(LoginRequiredMixin, ListView):

	def get(self, request):

		limit = int(request.GET.get('limit', 1))
		offset = int(request.GET.get('offset', 0))
		order = str(request.GET.get('order', ''))
		search = request.GET.get('search', '')
		sort = request.GET.get('sort', '')
		# data = {}
		orderby = ""
		if order != 'asc':
			orderby = "-"

		personas = Persona.objects.all().order_by('-id')

		if len(search) > 0:
			# personas = personas.filter(nombre__icontains=search)
			personas = Persona.objects.filter(
				Q(nombres__icontains=search) |
				Q(apepat__icontains=search) |
				Q(apemat__icontains=search) |
				Q(dni__icontains=search)
			)

		if sort != '':
			personas = personas.order_by(orderby + sort)

		paginador = Paginator(personas, limit)

		try:
			personas = paginador.page((offset / limit) + 1)
		except PageNotAnInteger:
			# If page is not an integer, deliver first page.
			personas = paginador.page(1)
		except EmptyPage:
			# If page is out of range (e.g. 9999), deliver last page of results.
			personas = paginador.page(paginador.num_pages)

		return render(
			request,
			'administracion/persona/datos.json',
			{
				'personas': personas,
				'total': paginador.count
			}
		)

class PersonaCreateView(AuditableMixin, CreateView):

	model = Persona
	form_class = PersonaForm
	template_name = "administracion/persona/formulario.html"

	def form_valid(self, form):

		super(PersonaCreateView, self).form_valid(form)
		msg = {}
		msg['msg'] = "Creado Correctamente"
		msg['value'] = True
		return HttpResponse(json.dumps(msg), content_type='application/json')

	def form_invalid(self, form):
		super(PersonaCreateView, self).form_invalid(form)
		return render(self.request, self.template_name, {'form': form})

class PersonaUpdateView(AuditableMixin, UpdateView):

	model = Persona
	form_class = PersonaForm
	template_name = "administracion/persona/formulario.html"

	def dispatch(self, *args, **kwargs):
		self.persona_id = kwargs['pk']
		return super(PersonaUpdateView, self).dispatch(*args, **kwargs)

	def form_valid(self, form):
		super(PersonaUpdateView, self).form_valid(form)
		persona = Persona.objects.get(pk=self.persona_id)
		msg = {}
		msg['msg'] = "Actualizado Correctamente"
		msg['value'] = True
		return HttpResponse(json.dumps(msg), content_type='application/json')

	def form_invalid(self, form):
		super(PersonaUpdateView, self).form_invalid(form)
		return render(self.request, self.template_name, {'form': form})

	def get_context_data(self, **kwargs):
		context = super(PersonaUpdateView, self).get_context_data(**kwargs)
		context["persona_id"] = self.persona_id
		return context

class PersonaDeleteView(DeleteView):
	model = Persona
	form_class = PersonaForm
	template_name = "administracion/persona/eliminar.html"

	def dispatch(self, *args, **kwargs):
		self.persona_id = kwargs['pk']
		return super(PersonaDeleteView, self).dispatch(*args, **kwargs)

	def get_success_url(self):
		pass

	def delete(self, request, *args, **kwargs):
		self.object = self.get_object()
		self.object.delete()
		msg = {}
		msg['msg'] = "Eliminado Correctamente"
		msg['value'] = True
		return HttpResponse(json.dumps(msg), content_type='application/json')

	def get_context_data(self, **kwargs):
		context = super(PersonaDeleteView, self).get_context_data(**kwargs)
		context["persona_id"] = self.persona_id
		return context

