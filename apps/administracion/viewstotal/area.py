from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse, HttpResponseRedirect
import json

from ...inicio.models import AuditableMixin
from ..models import Area
from ..formstotal.area import AreaForm
from ...inicio.mixin import LoginRequiredMixin

class AreaView(LoginRequiredMixin, TemplateView):
    template_name = "administracion/area/inicio.html"

class AreaListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        areas = Area.objects.all().order_by('-id')

        if len(search) > 0:
            areas = areas.filter(nombre__icontains=search)
            # profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

        if sort != '':
            areas = areas.order_by(orderby + sort)

        paginador = Paginator(areas, limit)

        try:
            areas = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            areas = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            areas = paginador.page(paginador.num_pages)

        return render(
            request,
            'administracion/area/datos.json',
            {
                'areas': areas,
                'total': paginador.count
            }
        )

class AreaCreateView(LoginRequiredMixin, AuditableMixin, CreateView):
    model = Area
    form_class = AreaForm
    template_name = "administracion/area/formulario.html"

    def form_valid(self, form):
        super(AreaCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(AreaCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class AreaUpdateView(LoginRequiredMixin, AuditableMixin, UpdateView):
    model = Area
    form_class = AreaForm
    template_name = "administracion/area/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.area_id = kwargs['pk']
        return super(AreaUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(AreaUpdateView, self).form_valid(form)
        area = Area.objects.get(pk=self.area_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(AreaUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(AreaUpdateView, self).get_context_data(**kwargs)
        context["area_id"] = self.area_id
        return context

class AreaDeleteView(LoginRequiredMixin, DeleteView):
    model = Area
    form_class = AreaForm
    template_name = "administracion/area/eliminar.html"

    def dispatch(self, *args, **kwargs):
        self.area_id = kwargs['pk']
        return super(AreaDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def get_context_data(self, **kwargs):
        context = super(AreaDeleteView, self).get_context_data(**kwargs)
        context["area_id"] = self.area_id
        return context

