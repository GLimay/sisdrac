# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.db import models
from ..inicio.models import Auditoria, Especialidad, Profesion, Provincia, Caserio, TipoOrganizacion
from ..inicio.funciones import CalcularEdad
from django.contrib.auth.models import User

# Create your models here.
class FuncionCargo(Auditoria):
    nombre = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name = 'Función - Cargo'
        verbose_name_plural = 'Funciones - Cargos'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""

class Area(Auditoria):
    nombre = models.CharField(max_length=150, unique=True)
    abreviatura = models.CharField(max_length=10, unique=True)
    estado = models.BooleanField(default=True)
    esagencia = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Area'
        verbose_name_plural = 'Areas'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""

class Organizacion(Auditoria):
    nombre = models.CharField(max_length=45)
    direccion = models.CharField(max_length=45)
    ruc = models.CharField(max_length=45, unique=True)
    area = models.DecimalField(max_digits=9, decimal_places=2)
    rendimiento = models.DecimalField(max_digits=9, decimal_places=2)
    volumen = models.DecimalField(max_digits=9, decimal_places=2)
    caserio = models.ForeignKey(Caserio, on_delete=models.PROTECT)
    tipoorganizacion = models.ForeignKey(TipoOrganizacion, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Organización'
        verbose_name_plural = 'Organizaciones'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""

class Persona(Auditoria):
    SexoM = 'M'
    SexoF = 'F'

    Sexos_Opciones = (
        (SexoM, u'Masculino'),
        (SexoF, u'Femenino')
    )

    dni = models.CharField(max_length=8, unique=True)
    nombres = models.CharField(max_length=50)
    apepat = models.CharField(max_length=50)
    apemat = models.CharField(max_length=50)
    sexo = models.CharField(max_length=1, default=SexoM, choices=Sexos_Opciones)
    fechanacimiento = models.DateField()
    user = models.OneToOneField(User, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'

    def __unicode__(self):
        return self.nombreCompleto()

    def get_absolute_url(self):
        return ""

    def estrabajador(self):
        _result = False
        for tra in self.trabajador_set.all():
            if tra.fechafin == None:
                _result = True
                break
        return _result

    def nombreCompleto(self):
        return self.nombres + ' ' + self.apepat + ' ' + self.apemat

    def Sexo(self):
        _result = ""
        for op in self.Sexos_Opciones:
            if op[0] == self.sexo:
                _result = op[1]
                break
        return _result

    def EdadActual(self):
        return CalcularEdad(self.fechanacimiento, datetime.datetime.now().date())

class Trabajador(Auditoria):
    persona = models.ForeignKey(Persona, on_delete=models.PROTECT)
    area = models.ForeignKey(Area, on_delete=models.PROTECT)
    funcioncargo = models.ForeignKey(FuncionCargo, on_delete=models.PROTECT)
    especialidad = models.ForeignKey(Especialidad, on_delete=models.PROTECT)
    profesion = models.ForeignKey(Profesion, on_delete=models.PROTECT)
    provincia = models.ForeignKey(Provincia, on_delete=models.PROTECT)
    direccion = models.CharField(max_length=45, blank=True)
    modalidad = models.CharField(max_length=45, blank=True)
    fechainicio = models.DateField()
    fechafin = models.DateField(blank=True, null=True)
    estado = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Trabajador'
        verbose_name_plural = 'Trabajadores'

    def __unicode__(self):
        return self.persona.nombreCompleto()

    def get_absolute_url(self):
        return ""

class Menu(Auditoria):
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=100, blank=True, null=True)
    ruta = models.CharField(max_length=100, blank=True, null=True)
    padre = models.ForeignKey('self', blank=True, null=True, related_name='menupadre', on_delete=models.PROTECT)
    orden = models.IntegerField(default=0)
    estado = models.BooleanField(default=True)
    separador = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Menú'
        verbose_name_plural = 'Menús'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""

    def Descripcion(self):
        return self.descripcion if self.descripcion != None and len(self.descripcion) > 0 else self.nombre

class Acceso(Auditoria):
    persona = models.ForeignKey(Persona, on_delete=models.PROTECT)
    menu = models.ForeignKey(Menu, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Acceso'
        verbose_name_plural = 'Accesos'

    def __unicode__(self):
        return self.persona.nombreCompleto()

    def get_absolute_url(self):
        return ""
