# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.db import models
from ..administracion.models import Area, Trabajador, Persona
from ..inicio.models import Auditoria, UnidadMedida, Mes, Caserio, TipoOrganizacion
from ..agricola.models import Base


# Create your models here.

class Funcion(Auditoria):
    nombre = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name = 'Función'
        verbose_name_plural = 'Funciones'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class DivisionFuncional(Auditoria):
    nombre = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name = 'División Funcional'
        verbose_name_plural = 'Divisiones Funcionales'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class GrupoFuncional(Auditoria):
    nombre = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name = 'Grupo Funcional'
        verbose_name_plural = 'Grupos Funcionales'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class Indicador(Auditoria):
    nombre = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name = 'Indicador'
        verbose_name_plural = 'Indicadores'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class Periodo(Auditoria):
    nombre = models.CharField(max_length=45, unique=True)
    estado = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Periodo'
        verbose_name_plural = 'Periodos'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class EjeEcon(Auditoria):
    nombre = models.CharField(max_length=75, unique=True)
    descripcion = models.TextField()
    estado = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Eje Económico'
        verbose_name_plural = 'Ejes Económicos'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class Pdrc(Auditoria):
    codigo = models.CharField(max_length=10, unique=True)
    descripcion = models.TextField()
    estado = models.BooleanField(default=True)
    periodo = models.ForeignKey(Periodo, on_delete=models.PROTECT)
    ejeecon = models.ForeignKey(EjeEcon, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Plan de Desarrollo'
        verbose_name_plural = 'Planes de Desarrollos'

    def __unicode__(self):
        return self.descripcion

    def get_absolute_url(self):
        return ""


class ObjetivoPdrc(Auditoria):
    codigo = models.CharField(max_length=10, unique=True)
    descripcion = models.TextField()
    estado = models.BooleanField(default=True)
    pdrc = models.ForeignKey(Pdrc, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Objetivo del PDRC'
        verbose_name_plural = 'Objetivos del PDRC'

    def __unicode__(self):
        return self.descripcion

    def get_absolute_url(self):
        return ""


class ObjetivoEspecifico(Auditoria):
    codigo = models.CharField(max_length=10, unique=True)
    descripcion = models.TextField()
    estado = models.BooleanField(default=True)
    objetivopdrc = models.ForeignKey(ObjetivoPdrc, on_delete=models.PROTECT)
    anio = models.IntegerField()

    class Meta:
        verbose_name = 'Objetivo Específico'
        verbose_name_plural = 'Objetivos Específicos'

    def __unicode__(self):
        return self.descripcion

    def get_absolute_url(self):
        return ""


class AccionEstrategica(Auditoria):
    codigo = models.CharField(max_length=45, unique=True)
    nombre = models.TextField()
    objetivoespecifico = models.ForeignKey(ObjetivoEspecifico, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Acción Estratégica'
        verbose_name_plural = 'Acciones Estratégicas'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class CadenaProductiva(Auditoria):
    cadProd = (
        ('CU', u'Cultivo'),
        ('CR', u'Crianza')
    )

    nombre = models.CharField(max_length=255, unique=True)
    tipo = models.CharField(max_length=2, default=cadProd[0][0])
    estado = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Cadena Productiva'
        verbose_name_plural = 'Cadenas Productivas'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""

    def Tipo(self):
        _result = ""
        for op in self.cadProd:
            if op[0] == self.tipo:
                _result = op[1]
                break
        return _result


class ProgramaPresupuestal(Auditoria):
    codigo = models.CharField(max_length=45, unique=True)
    nombre = models.CharField(max_length=100, unique=True)
    descripcion = models.TextField()
    unidadmedida = models.ForeignKey(UnidadMedida, on_delete=models.PROTECT)
    indicador = models.ForeignKey(Indicador, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Programa Presupuestal'
        verbose_name_plural = 'Programas Presupuestales'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class ProductoProyecto(Auditoria):
    tipoProdProy = (
        ('PR', u'Producto'),
        ('PY', u'Proyecto')
    )

    codigo = models.CharField(max_length=45, unique=True)
    nombre = models.CharField(max_length=100, unique=True)
    descripcion = models.TextField()
    indicador = models.ForeignKey(Indicador, on_delete=models.PROTECT)
    unidadmedida = models.ForeignKey(UnidadMedida, on_delete=models.PROTECT)
    programapresupuestal = models.ForeignKey(ProgramaPresupuestal, on_delete=models.PROTECT)
    tipo = models.CharField(max_length=2, default=tipoProdProy[0][0])

    class Meta:
        verbose_name = 'Producto de Proyecto'
        verbose_name_plural = 'Productos de Proyectos'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""

    def Tipo(self):
        _result = ""
        for op in self.tipoProdProy:
            if op[0] == self.tipo:
                _result = op[1]
                break
        return _result


class AeXPop(Auditoria):
    accionestrategica = models.ForeignKey(AccionEstrategica, on_delete=models.PROTECT)
    productoproyecto = models.ForeignKey(ProductoProyecto, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Aexpop'
        verbose_name_plural = 'Aexpop'

    def __unicode__(self):
        return self.accionestrategica.nombre + '/' + self.productoproyecto.nombre

    def get_absolute_url(self):
        return ""


class ActividadCatalogo(Auditoria):
    codigo = models.CharField(max_length=10, unique=True, default="")
    nombre = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name = 'Catálogo de Actividad'
        verbose_name_plural = 'Catálogos de Actividades'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""

class Actividad(Auditoria):
    actividadcatalogo = models.ForeignKey(ActividadCatalogo)
    area = models.ForeignKey(Area, on_delete=models.PROTECT)
    funcion = models.ForeignKey(Funcion, on_delete=models.PROTECT)
    divisionfuncional = models.ForeignKey(DivisionFuncional, on_delete=models.PROTECT)
    grupofuncional = models.ForeignKey(GrupoFuncional, on_delete=models.PROTECT)
    cadenaproductiva = models.ForeignKey(CadenaProductiva, on_delete=models.PROTECT)
    aexpop = models.ForeignKey(AeXPop, on_delete=models.PROTECT)
    anio = models.IntegerField(default=datetime.datetime.now().year)
    indicador = models.ForeignKey(Indicador)
    unidadmedida = models.ForeignKey(UnidadMedida)

    class Meta:
        verbose_name = 'Actividad'
        verbose_name_plural = 'Actividades'

    def __unicode__(self):
        return self.actividadcatalogo.nombre

    def get_absolute_url(self):
        return ""


class MetaFF(Auditoria):
    metafisica_programada = models.IntegerField(null=True, blank=True)
    metafinanciera_programada = models.DecimalField(max_digits=11, decimal_places=2, null=True, blank=True)
    metafisica_ejecutada = models.IntegerField(null=True, blank=True)
    metafinanciera_ejecutada = models.DecimalField(max_digits=11, decimal_places=2, null=True, blank=True)
    mes = models.ForeignKey(Mes, on_delete=models.PROTECT)
    actividad = models.ForeignKey(Actividad, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Meta Física Financiera'
        verbose_name_plural = 'Metas Físicas Financieras'

    def __unicode__(self):
        return self.mes.nombre + " - " + str(self.metafisica_programada) + "-" + str(self.metafinanciera_programada)

    def get_absolute_url(self):
        return ""


class AccionObra(Auditoria):
    km = models.DecimalField(max_digits=11, decimal_places=2)
    volumen = models.DecimalField(max_digits=11, decimal_places=2)
    conduce = models.DecimalField(max_digits=11, decimal_places=2)
    presupuesto = models.DecimalField(max_digits=11, decimal_places=2)
    ha = models.DecimalField(max_digits=11, decimal_places=2)
    caserio = models.ForeignKey(Caserio, on_delete=models.PROTECT)
    cadenaproductiva = models.ForeignKey(CadenaProductiva, on_delete=models.PROTECT)
    aexpop = models.ForeignKey(AeXPop, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Acción Obra'
        verbose_name_plural = 'Acciones Obras'

    def __unicode__(self):
        return str(self.km)

    def get_absolute_url(self):
        return ""


class SubActividadCatalogo(Auditoria):
    nombre = models.CharField(max_length=100, unique=True)

    class Meta:
        verbose_name = 'Catálogo de Sub Actividad'
        verbose_name_plural = 'Catálogos de Sub Actividades'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""


class SubActividadComponente(Auditoria):
    codigo = models.CharField(max_length=45, unique=True)
    subactividadcatalogo = models.ForeignKey(SubActividadCatalogo)
    unidadmedida = models.ForeignKey(UnidadMedida, on_delete=models.PROTECT)
    indicador = models.ForeignKey(Indicador, on_delete=models.PROTECT)
    actividad = models.ForeignKey(Actividad, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Sub Actividad de Componente'
        verbose_name_plural = 'Sub Actividades de Componentes'

    def __unicode__(self):
        return self.subactividadcatalogo.nombre

    def get_absolute_url(self):
        return ""


def content_file_name_TareaCpCu(instance, filename):
    return '/'.join(['tareacpcu', instance.trabajador.nombre, 'evidencia', filename])


def content_file_name_TareaCpCu2(instance, filename):
    return '/'.join(['tareacpcu', instance.trabajador.nombre, 'adjunto', filename])


class TareaCatalogo(Auditoria):
    nombre = models.CharField(max_length=200, unique=True)

    class Meta:
        verbose_name = 'Catálogo de Tarea'
        verbose_name_plural = 'Catálogos de Tareas'

    def __unicode__(self):
        return self.nombre

    def get_absolute_url(self):
        return ""

class TareaCpCu(Auditoria):
    tareacatalogo = models.ForeignKey(TareaCatalogo)
    referencia = models.TextField(blank=True, null=True)
    trabajador = models.ForeignKey(Trabajador, on_delete=models.PROTECT)
    subactividadcomponente = models.ForeignKey(SubActividadComponente, on_delete=models.PROTECT)
    base = models.ForeignKey(Base, on_delete=models.PROTECT)
    prioridad = models.CharField(max_length=20, blank=True, null=True)
    estado = models.CharField(max_length=30, blank=True, null=True)
    porcentaje_completado = models.DecimalField(blank=True, null=True, decimal_places=2, max_digits=11)
    descripcion = models.TextField(blank=True, null=True)
    inicio = models.DateField(blank=True, null=True)
    fin = models.DateField(blank=True, null=True)
    evidencia = models.ImageField(upload_to=content_file_name_TareaCpCu, max_length=1000, blank=True, null=True)
    adjunto = models.ImageField(upload_to=content_file_name_TareaCpCu2, max_length=1000, blank=True, null=True)

    class Meta:
        verbose_name = 'Tarea CpCu'
        verbose_name_plural = 'Tareas CpCu'

    def __unicode__(self):
        return self.trabajador.persona.nombreCompleto()

    def get_absolute_url(self):
        return ""




def content_file_name_TareaCpCr(instance, filename):
    return '/'.join(['tareacpcr', instance.trabajador.nombre, 'evidencia', filename])


def content_file_name_TareaCpCr2(instance, filename):
    return '/'.join(['tareacpcr', instance.trabajador.nombre, 'adjunto', filename])


class TareaCpCr(Auditoria):
    nombre = models.CharField(max_length=300)
    prioridad = models.CharField(max_length=20, blank=True, null=True)
    estado = models.CharField(max_length=30, blank=True, null=True)
    porcentaje_completado = models.DecimalField(blank=True, null=True, decimal_places=2, max_digits=11)
    descripcion = models.TextField(blank=True, null=True)
    inicio = models.DateField(blank=True, null=True)
    fin = models.DateField(blank=True, null=True)
    evidencia = models.ImageField(upload_to=content_file_name_TareaCpCr, max_length=1000, blank=True, null=True)
    adjunto = models.ImageField(upload_to=content_file_name_TareaCpCr2, max_length=1000, blank=True, null=True)
    persona = models.ForeignKey(Persona, on_delete=models.PROTECT)
    subactividadcomponente = models.ForeignKey(SubActividadComponente, on_delete=models.PROTECT)
    base = models.ForeignKey(Base, on_delete=models.PROTECT)
    tema = models.CharField(max_length=150, blank=True, null=True)
    metodologia = models.CharField(max_length=100, blank=True, null=True)
    desarrollo_tema = models.CharField(max_length=200, blank=True, null=True)
    requerimiento = models.CharField(max_length=200, blank=True, null=True)
    compromiso = models.CharField(max_length=200, blank=True, null=True)
   
    class Meta:
        verbose_name = 'Tarea CpCr'
        verbose_name_plural = 'Tareas CpCr'

    def __unicode__(self):
        return self.persona.nombres

    def get_absolute_url(self):
        related_name='' ""





