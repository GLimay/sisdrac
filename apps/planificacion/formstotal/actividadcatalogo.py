from django.forms import ModelForm

from ..models import ActividadCatalogo

class ActividadCatalogoForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(ActividadCatalogoForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = ActividadCatalogo
		fields = ['codigo', 'nombre']

