# -*- coding: utf-8 -*-
from django import forms
from django.utils.encoding import force_text
from django_select2.forms import ModelSelect2Widget, Select2Widget

from ..models import ProductoProyecto, AccionEstrategica, AeXPop, ProgramaPresupuestal

class widgetProgPptal(ModelSelect2Widget):
    search_fields = ["codigo__icontains", "nombre__icontains"]
    queryset = ProgramaPresupuestal.objects.order_by("nombre")
    max_results = 10

class widgetProdProy(ModelSelect2Widget):
    search_fields=["codigo__icontains", "nombre__icontains"]
    max_results=10

    def label_from_instance(self, obj):
        return force_text(obj.codigo) + " " + force_text(obj.nombre)

class AeXPopProgPptalForm(forms.Form):
    opprogpptal = forms.ModelChoiceField(
        queryset=ProgramaPresupuestal.objects.all(),
        label=u'Programa Presupuestal',
        widget=widgetProgPptal(
            attrs={'id': 'cb_progpptal', 'class': 'formProgPptal_load', 'data-allow-clear': 'false'},
        )
    )

    optipo = forms.ChoiceField(
        label=u"Tipo",
        choices=ProductoProyecto.tipoProdProy,
        widget=Select2Widget(attrs={'id': 'cb_tipo', 'class': 'formProgPptal_load', 'data-minimum-results-for-search': 'Infinity'})
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(AeXPopProgPptalForm, self).__init__(*args, **kwargs)

class AeXPopProdProyForm(forms.Form):
    opprodproy = forms.ChoiceField(
        required=False,
        widget=widgetProdProy(
            attrs={'id': 'cb_prodproy', 'class': 'formProdProy_load', 'data-allow-clear': 'false'}
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(AeXPopProdProyForm, self).__init__(*args, **kwargs)
        self.fields["opprodproy"].label = u'Producto' if self.request.GET.get("optipo", "").upper() == "PR" else u'Proyecto'
        self.fields["opprodproy"].widget.queryset = ProductoProyecto.objects.filter(
            tipo=self.request.GET.get("optipo", "").upper(),
            programapresupuestal_id=self.request.GET.get("opprogpptal", 0)
        ).order_by("nombre")

class widgetAccionEstrategica(ModelSelect2Widget):
    search_fields=["codigo__icontains", "nombre__icontains"]
    max_results=10
    queryset = AccionEstrategica.objects.order_by("codigo")

    def label_from_instance(self, obj):
        return force_text(obj.codigo) + " " + force_text(obj.nombre)

class AeXPopForm(forms.ModelForm):

    accionestrategica = forms.ModelChoiceField(
        queryset=AccionEstrategica.objects.all(),
        label=u'Acción Estratégica',
        widget=widgetAccionEstrategica()
    )

    productoproyectoid = forms.IntegerField(
        widget=forms.HiddenInput
    )

    def __init__(self, *args, **kwargs):
        super(AeXPopForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = AeXPop
        fields = ['accionestrategica']

    def clean(self):
        self.instance.productoproyecto_id = self.cleaned_data["productoproyectoid"]
        ae = self.cleaned_data.get("accionestrategica", None)
        if ae:
            accest = AeXPop.objects.filter(
                productoproyecto_id=self.instance.productoproyecto_id,
                accionestrategica_id=ae.id
            ).exclude(
                pk=self.instance.pk
            )
            if accest.count() > 0:
                self.add_error("accionestrategica", u"La acción estratégica ya fue seleccionada")


