from django.forms import ModelForm
from ..models import Indicador

class IndicadorForm(ModelForm):

	def __init__(self, *args, **kwargs):
		super(IndicadorForm, self).__init__(*args, **kwargs)
		for i, (fname, field) in enumerate(self.fields.iteritems()):
			field.widget.attrs['class'] = 'form-control'

	class Meta:
		model = Indicador
		fields = ['nombre']

