from django.forms import ModelForm
from django import forms
from django.utils.encoding import force_text
from django_select2.forms import ModelSelect2Widget

from ..models import Periodo, EjeEcon, Pdrc, ObjetivoPdrc, ObjetivoEspecifico, AccionEstrategica

class widgetOpPdrc(ModelSelect2Widget):
    search_fields=["codigo__icontains", "descripcion__icontains"]
    queryset=Pdrc.objects.order_by("descripcion")
    model=Pdrc
    max_results=10

    def label_from_instance(self, obj):
        return force_text(obj.descripcion) + " (" + force_text(obj.periodo.nombre) + "-" + force_text(obj.ejeecon.nombre) + ")"

class widgetObjPdrc(ModelSelect2Widget):
    search_fields=["codigo__icontains", "descripcion__icontains"]
    model=ObjetivoPdrc
    max_results=10

    def label_from_instance(self, obj):
        return force_text(obj.codigo) + " " + force_text(obj.descripcion)

class widgetObjEspPdrc(ModelSelect2Widget):
    search_fields=["codigo__icontains", "descripcion__icontains"]
    model=ObjetivoEspecifico
    max_results=10

    def label_from_instance(self, obj):
        return force_text(obj.codigo) + " " + force_text(obj.descripcion)

class AccionEstrategicaPdrcForm(forms.Form):
    oppdrc = forms.ChoiceField(
        required=False,
        label=u'Plan de Desarrollo',
        widget=widgetOpPdrc(
            attrs={'id': 'cb_pdrc', 'class': 'formPdrc_load', 'data-allow-clear': 'false'}
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(AccionEstrategicaPdrcForm, self).__init__(*args, **kwargs)

class AccionEstrategicaObjPdrcForm(forms.Form):
    opobjpdrc = forms.ChoiceField(
        required=False,
        label=u'Objetivo General',
        widget=widgetObjPdrc(
            attrs={'id': 'cb_objpdrc', 'class': 'formObjPdrc_load', 'data-allow-clear': 'false'}
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(AccionEstrategicaObjPdrcForm, self).__init__(*args, **kwargs)
        self.fields["opobjpdrc"].widget.queryset = ObjetivoPdrc.objects.filter(
            pdrc_id=self.request.GET.get("oppdrc", 0)
        ).order_by("descripcion")

class AccionEstrategicaObjEspPdrcForm(forms.Form):
    opobjesppdrc = forms.ChoiceField(
        required=False,
        label=u'Objetivo Especifico',
        widget=widgetObjEspPdrc(
            attrs={'id': 'cb_objesppdrc', 'class': 'formObjEspPdrc_load', 'data-allow-clear': 'false'}
        )
    )

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(AccionEstrategicaObjEspPdrcForm, self).__init__(*args, **kwargs)
        self.fields["opobjesppdrc"].widget.queryset = ObjetivoEspecifico.objects.filter(
            objetivopdrc_id=self.request.GET.get("opobjpdrc", 0)
        ).order_by("descripcion")

class AccionEstrategicaForm(ModelForm):

    objetivoespecifico_id = forms.IntegerField(
        widget=forms.HiddenInput
    )

    def __init__(self, *args, **kwargs):
        super(AccionEstrategicaForm, self).__init__(*args, **kwargs)
        for i, (fname, field) in enumerate(self.fields.iteritems()):
            field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = AccionEstrategica
        fields = ['codigo', 'nombre']

    def clean(self):
        self.instance.objetivoespecifico_id = self.cleaned_data["objetivoespecifico_id"]

