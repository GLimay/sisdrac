import json

from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, DetailView, ListView, CreateView, UpdateView, DeleteView,TemplateView

from ...inicio.models import AuditableMixin
from ..models import ProgramaPresupuestal
from ..formstotal.programapresupuestal import ProgramaPresupuestalForm
from ...inicio.mixin import LoginRequiredMixin


class ProgramaPresupuestalView(LoginRequiredMixin, TemplateView):
    template_name = "planificacion/programapresupuestal/inicio.html"

class ProgramaPresupuestalListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):

        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')

        orderby = ""
        if order != 'asc':
            orderby = "-"

        programapresupuestal = ProgramaPresupuestal.objects.all()
        if len(search) > 0:
            programapresupuestal = programapresupuestal.filter(
                Q(nombre__icontains=search) |
                Q(codigo__icontains=search) |
                Q(descripcion__icontains=search)
            )

        if sort != '':
            programapresupuestal = programapresupuestal.order_by(orderby + sort)

        paginador = Paginator(programapresupuestal, limit)

        try:
            programapresupuestal = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            programapresupuestal = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            programapresupuestal = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/programapresupuestal/datos.json',
            {
                'programapresupuestal': programapresupuestal,
                'total': paginador.count
            }
        )

class ProgramaPresupuestalCreateView(AuditableMixin, CreateView):
    model = ProgramaPresupuestal
    form_class = ProgramaPresupuestalForm
    template_name = "planificacion/programapresupuestal/formulario.html"

    def form_valid(self, form):
        super(ProgramaPresupuestalCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(ProgramaPresupuestalCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class ProgramaPresupuestalUpdateView(AuditableMixin, UpdateView):
    model = ProgramaPresupuestal
    form_class = ProgramaPresupuestalForm
    template_name = "planificacion/programapresupuestal/formulario.html"

    def form_valid(self, form):
        super(ProgramaPresupuestalUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(ProgramaPresupuestalUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class ProgramaPresupuestalDeleteView(DeleteView):
    model = ProgramaPresupuestal
    form_class = ProgramaPresupuestalForm
    template_name = "planificacion/programapresupuestal/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')
