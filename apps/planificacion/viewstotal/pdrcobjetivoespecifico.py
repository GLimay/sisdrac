import json

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import FormView, DetailView, ListView, CreateView, UpdateView, DeleteView

from ...inicio.models import AuditableMixin
from ..models import ObjetivoPdrc, ObjetivoEspecifico, AccionEstrategica
from ..formstotal.objetivoespecifico import ObjetivoEspecificoPdrcForm, ObjetivoEspecificoObjPdrcForm, \
    ObjetivoEspecificoForm
from ...inicio.mixin import LoginRequiredMixin


class ObjetivoEspecificoPdrcView(LoginRequiredMixin, FormView):
    template_name = "planificacion/pdrcobjesp/pdrc.html"
    form_class = ObjetivoEspecificoPdrcForm

    def get_form_kwargs(self):
        kwargs = super(ObjetivoEspecificoPdrcView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(ObjetivoEspecificoPdrcView, self).get_context_data(**kwargs)
        data['formPdrc'] = data.get('form')
        return data


class ObjetivoEspecificoObjPdrcView(LoginRequiredMixin, FormView):

    template_name = "planificacion/pdrcobjesp/objpdrc.html"
    form_class = ObjetivoEspecificoObjPdrcForm

    def get_form_kwargs(self):
        kwargs = super(ObjetivoEspecificoObjPdrcView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_context_data(self, **kwargs):
        data = super(ObjetivoEspecificoObjPdrcView, self).get_context_data(**kwargs)
        data['formObjPdrc'] = data.get('form')
        return data


class PdrcObjEspView(LoginRequiredMixin, DetailView):
    template_name = "planificacion/pdrcobjesp/inicio.html"
    context_object_name = "oObjetivo"
    model = ObjetivoPdrc


class PdrcObjEspListView(LoginRequiredMixin, ListView):
    def get(self, request, *args, **kwargs ):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        objId = kwargs.get('pkesp', 0)
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        pdrcObjEsp = ObjetivoEspecifico.objects.filter(objetivopdrc_id=objId).order_by('-id')

        if len(search) > 0:
            pdrcObjEsp = pdrcObjEsp.filter(
                Q(descripcion__icontains=search) |
                Q(codigo__icontains=search)
            )

        if sort != '':
            pdrcObjEsp = pdrcObjEsp.order_by(orderby + sort)

        paginador = Paginator(pdrcObjEsp, limit)

        try:
            pdrcObjEsp = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            pdrcObjEsp = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            pdrcObjEsp = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/pdrcobjesp/datos.json',
            {
                'pdrcobjesp': pdrcObjEsp,
                'total': paginador.count
            }
        )


class ObjetivoEspecificoCreateView(AuditableMixin, CreateView):
    model = ObjetivoEspecifico
    form_class = ObjetivoEspecificoForm
    template_name = "planificacion/pdrcobjesp/formulario.html"

    def form_valid(self, form):
        super(ObjetivoEspecificoCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(ObjetivoEspecificoCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class ObjetivoEspecificoUpdateView(AuditableMixin, UpdateView):
    model = ObjetivoEspecifico
    form_class = ObjetivoEspecificoForm
    template_name = "planificacion/pdrcobjesp/formulario.html"

    def form_valid(self, form):
        super(ObjetivoEspecificoUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(ObjetivoEspecificoUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class ObjetivoEspecificoDeleteView(DeleteView):
    model = ObjetivoEspecifico
    form_class = ObjetivoEspecificoForm
    template_name = "planificacion/pdrcobjesp/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')
