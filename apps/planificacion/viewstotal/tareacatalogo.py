from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse
import json

from ..models import TareaCatalogo
from ...inicio.models import AuditableMixin
from ..formstotal.tareacatalogo import TareaCatalogoForm
from ...inicio.mixin import LoginRequiredMixin

class TareaCatalogoView(LoginRequiredMixin, TemplateView):
    template_name = "planificacion/tareacatalogo/inicio.html"

class TareaCatalogoListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        tareascatalogos = TareaCatalogo.objects.all().order_by('-id')

        if len(search) > 0:
            tareascatalogos = tareascatalogos.filter(nombre__icontains=search)

        if sort != '':
            tareascatalogos = tareascatalogos.order_by(orderby + sort)

        paginador = Paginator(tareascatalogos, limit)

        try:
            tareascatalogos = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            tareascatalogos = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            tareascatalogos = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/tareacatalogo/datos.json',
            {
                'tareascatalogos': tareascatalogos,
                'total': paginador.count
            }
        )

class TareaCatalogoCreateView(AuditableMixin, CreateView):
    model = TareaCatalogo
    form_class = TareaCatalogoForm
    template_name = "planificacion/actividadcatalogo/formulario.html"

    def form_valid(self, form):
        super(TareaCatalogoCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(TareaCatalogoCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class TareaCatalogoUpdateView(AuditableMixin, UpdateView):
    model = TareaCatalogo
    form_class = TareaCatalogoForm
    template_name = "planificacion/tareacatalogo/formulario.html"

    def form_valid(self, form):
        super(TareaCatalogoUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(TareaCatalogoUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class TareaCatalogoDeleteView(DeleteView):
    model = TareaCatalogo
    form_class = TareaCatalogoForm
    template_name = "planificacion/tareacatalogo/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')



