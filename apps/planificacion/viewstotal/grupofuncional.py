from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse, HttpResponseRedirect
import json

from ...inicio.models import AuditableMixin
from ..models import GrupoFuncional
from ..formstotal.grupofuncional import GrupoFuncionalForm
from ...inicio.mixin import LoginRequiredMixin

class GrupoFuncionalView(LoginRequiredMixin, TemplateView):
    template_name = "planificacion/grupofuncional/inicio.html"

class GrupoFuncionalListView(LoginRequiredMixin, ListView):

    def get(self, request):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        grupofuncional = GrupoFuncional.objects.all().order_by('-id')

        if len(search) > 0:
            grupofuncional = grupofuncional.filter(nombre__icontains=search)
            # profesion = Profesion.objects.filter(Q(nombre__startswith=search) | Q(apellido__startswith=search) | Q(dni__startswith=search))

        if sort != '':
            grupofuncional = grupofuncional.order_by(orderby + sort)

        paginador = Paginator(grupofuncional, limit)

        try:
            grupofuncional = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            grupofuncional = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            grupofuncional = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/grupofuncional/datos.json',
            {
                'grupofuncional': grupofuncional,
                'total': paginador.count
            }
        )

class GrupoFuncionalCreateView(LoginRequiredMixin, AuditableMixin, CreateView):
    model = GrupoFuncional
    form_class = GrupoFuncionalForm
    template_name = "planificacion/grupofuncional/formulario.html"

    def form_valid(self, form):
        super(GrupoFuncionalCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(GrupoFuncionalCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class GrupoFuncionalUpdateView(LoginRequiredMixin, AuditableMixin, UpdateView):
    model = GrupoFuncional
    form_class = GrupoFuncionalForm
    template_name = "planificacion/grupofuncional/formulario.html"

    def dispatch(self, *args, **kwargs):
        self.grupofuncional_id = kwargs['pk']
        return super(GrupoFuncionalUpdateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        super(GrupoFuncionalUpdateView, self).form_valid(form)
        grupofuncional = GrupoFuncional.objects.get(pk=self.grupofuncional_id)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(GrupoFuncionalUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

    def get_context_data(self, **kwargs):
        context = super(GrupoFuncionalUpdateView, self).get_context_data(**kwargs)
        context["grupofuncional_id"] = self.grupofuncional_id
        return context

class GrupoFuncionalDeleteView(LoginRequiredMixin, DeleteView):
    model = GrupoFuncional
    form_class = GrupoFuncionalForm
    template_name = "planificacion/grupofuncional/eliminar.html"

    def dispatch(self, *args, **kwargs):
        self.grupofuncional_id = kwargs['pk']
        return super(GrupoFuncionalDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def get_context_data(self, **kwargs):
        context = super(GrupoFuncionalDeleteView, self).get_context_data(**kwargs)
        context["grupofuncional_id"] = self.grupofuncional_id
        return context

