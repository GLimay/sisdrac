from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponse
import json

from ..models import SubActividadCatalogo
from ...inicio.models import AuditableMixin
from ..formstotal.subactividadcatalogo import SubActividadCatalogoForm
from ...inicio.mixin import LoginRequiredMixin

class SubActividadCatalogoView(LoginRequiredMixin, TemplateView):
    template_name = "planificacion/subactividadcatalogo/inicio.html"

class SubActividadCatalogoListView(LoginRequiredMixin, ListView):

    def get(self, request, *args, **kwargs):
        limit = int(request.GET.get('limit', 1))
        offset = int(request.GET.get('offset', 0))
        order = str(request.GET.get('order', ''))
        search = request.GET.get('search', '')
        sort = request.GET.get('sort', '')
        # data = {}
        orderby = ""
        if order != 'asc':
            orderby = "-"

        subactividadescatalogos = SubActividadCatalogo.objects.all().order_by('-id')

        if len(search) > 0:
            subactividadescatalogos = subactividadescatalogos.filter(nombre__icontains=search)

        if sort != '':
            subactividadescatalogos = subactividadescatalogos.order_by(orderby + sort)

        paginador = Paginator(subactividadescatalogos, limit)

        try:
            subactividadescatalogos = paginador.page((offset / limit) + 1)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            subactividadescatalogos = paginador.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            subactividadescatalogos = paginador.page(paginador.num_pages)

        return render(
            request,
            'planificacion/subactividadcatalogo/datos.json',
            {
                'subactividadescatalogos': subactividadescatalogos,
                'total': paginador.count
            }
        )

class SubActividadCatalogoCreateView(AuditableMixin, CreateView):
    model = SubActividadCatalogo
    form_class = SubActividadCatalogoForm
    template_name = "planificacion/actividadcatalogo/formulario.html"

    def form_valid(self, form):
        super(SubActividadCatalogoCreateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Creado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(SubActividadCatalogoCreateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class SubActividadCatalogoUpdateView(AuditableMixin, UpdateView):
    model = SubActividadCatalogo
    form_class = SubActividadCatalogoForm
    template_name = "planificacion/subactividadcatalogo/formulario.html"

    def form_valid(self, form):
        super(SubActividadCatalogoUpdateView, self).form_valid(form)
        msg = {}
        msg['msg'] = "Actualizado Correctamente"
        msg['value'] = True
        return HttpResponse(json.dumps(msg), content_type='application/json')

    def form_invalid(self, form):
        super(SubActividadCatalogoUpdateView, self).form_invalid(form)
        return render(self.request, self.template_name, {'form': form})

class SubActividadCatalogoDeleteView(DeleteView):
    model = SubActividadCatalogo
    form_class = SubActividadCatalogoForm
    template_name = "planificacion/subactividadcatalogo/eliminar.html"

    def get_success_url(self):
        pass

    def delete(self, request, *args, **kwargs):
        msg = {}
        msg['msg'] = "Eliminado Correctamente"
        msg['value'] = True
        try:
            self.get_object().delete()
        except Exception as e:
            msg['value'] = False
            return render(request, self.template_name, {'object': self.get_object(), 'delete_error': str(e)})
        return HttpResponse(json.dumps(msg), content_type='application/json')



